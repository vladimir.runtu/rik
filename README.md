# Proovitöö

*See repo sisaldab proovitööd, millega kontrollitakse minu teadmisi Python keelest, objekt-orienteeritud programmeerimise üldtunnustatud arendusmustrite/võtete tundmise ja kasutamise oskuseid ning oskust enda loodava tarkvara dokumenteerimiseks ja kvaliteedi kontrollimiseks*

## Kasutatud tehnoloogiad

* [FastAPI](https://fastapi.tiangolo.com/)
* [SQLAlchemy](https://www.sqlalchemy.org/)
  * [SQLite](https://www.sqlite.org/index.html)
* [Docker](https://www.docker.com/)
* [VuejS](https://vuejs.org/)
* [Bootstrap](https://getbootstrap.com/)

## Tarkvara arhitektuur

Ülesehitus on jaotatud kahte ossa - *frontend* ja *backend (API)*

Backend on üles ehitatud kasutates khilist arhitektuuri [Onion Architecture](https://jeffreypalermo.com/2008/07/the-onion-architecture-part-1/) ja DDD põhimõtteid :

```tree
├── rik_backend
│   └─ app
│      ├── command
│      ├── domain
│      │   ├── model
│      │   └── value_object
│      ├── infrastructure
│      │   └── sqlite
│      ├── query
│      └── tests
├── rik_frontend
```

## Käivitamine

Eelduseks on Dockeri olemasolu

1. Klooni antud repo
2. Mine kloonitud repo kataloogi ning käivita käsurealt `docker-compose up -d`
4. Juurdepääs *backend* API dokumentatsioonile avaneb aadressil http://127.0.0.1:5000/docs
4. Juurdepääs esilehele avaneb aadressil http://127.0.0.1:8080/

Lahenduse seiskamiseks käivita käsurealt `docker-compose down`