import { createApp } from 'vue';
import VueDatePicker from '@vuepic/vue-datepicker';
import App from './App.vue';
import router from './router';
import '@vuepic/vue-datepicker/dist/main.css';
import 'bootstrap/dist/css/bootstrap.css';

const app = createApp(App);
app.component('VueDatePicker', VueDatePicker);
app.use(router).mount('#app');
