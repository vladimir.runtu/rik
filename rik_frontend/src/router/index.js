import { createRouter, createWebHistory } from 'vue-router';
import Home from '../components/Home.vue';
import NewEvent from '../components/NewEvent.vue';
import AddGuest from '../components/AddGuest.vue';
import EventDetails from '../components/EventDetails.vue';

const routes = [
  { path: '/', name: 'Home', component: Home },
  { path: '/newevent', name: 'NewEvent', component: NewEvent },
  { path: '/event/:event_uid', component: EventDetails, name: 'eventdetails' },
  { path: '/event/:event_uid/addguest', component: AddGuest, name: 'addguest' },
  { path: '/event/:event_uid/rmguest/:guest_id', component: AddGuest, name: 'rmguest' },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
