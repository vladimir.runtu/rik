import pytest

from uuid import UUID, uuid4
from app.domain.model import üritus
from app.infrastructure.sqlite import üritus_dto

class test_dto:

    def test_to_read_model_should_create_entity_instance(self):

        uid: UUID=uuid4()

        ürr_dto = üritus_dto(
            uid=uid,
            name="test",
            created_at=1614007224642,
            updated_at=1614007224642,
        )

        rmodel = üritus_dto.to_read_model()

        assert rmodel.uid == uid
        assert rmodel.name=="test"