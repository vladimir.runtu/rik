import pytest

import uuid
from datetime import datetime, timedelta
from app.domain.model import eraisik, üritus, külaline
from app.domain.value_object import ee_isikukood, ee_registrikood, makseviis

class TestDomain:

    enimi: str = "TestEesNimi"
    pnimi: str = "TestPereNimi"
    ikood: str = "30000000000"

    def test_eraisik_constructor_should_create_instance(self):

        ikood = ee_isikukood(self.ikood)
        eisik = eraisik(eesnimi = self.enimi, perekonnanimi=self.pnimi, isikukood = ikood)

        assert eisik.eesnimi == self.enimi
        assert eisik.perekonnanimi == self.pnimi
        assert eisik.isikukood == ikood

    def test_fail_üritus_on_minevikus(self):

        d = datetime.today() - timedelta(days=1)
        with pytest.raises(Exception):
            üritus(uid=uuid.uuid4(), nimi="Test", aeg=d , koht="Tallinn", lisainfo="info")

    def test_fail_isikukood(self):

        with pytest.raises(Exception):
            ee_isikukood("123")

    def test_fail_registrikood(self):

        with pytest.raises(Exception):
            ee_registrikood("123")

    def test_lisa_ja_kustuta_külalised(self):

        d = datetime.today() + timedelta(days=1)

        ürr = üritus(uid=uuid.uuid4(), nimi="Test", aeg=d , koht="Tallinn", lisainfo="info", created_at=datetime.now(), updated_at=datetime.now())

        ikood = ee_isikukood(self.ikood)
        eisik = eraisik(eesnimi = self.enimi, perekonnanimi=self.pnimi, isikukood = ikood)

        k = külaline(isik=eisik, makseviis=makseviis.SULARAHA, lisainfo="info")
        ürr.lisa_külaline(külaline = k)
        ürr.lisa_külaline(külaline = k)

        assert ürr.külaliste_arv() == 1

        ürr.kustuta_külaline(külaline = k)

        assert ürr.külaliste_arv() == 0
