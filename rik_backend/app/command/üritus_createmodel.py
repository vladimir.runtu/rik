from pydantic import BaseModel, Field, validator
from datetime import datetime

class üritus_createmodel(BaseModel):

    nimi: str = Field(example="Üritus")
    koht: str = Field(example="Tallinn")
    lisainfo: str = Field(example="Lisainfo")
    aeg: datetime = Field(example="2024-01-01T10:00")