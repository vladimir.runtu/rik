from pydantic import BaseModel, Field, validator
from datetime import datetime

class külaline_createmodel(BaseModel):

    eesnimi: str = Field(example="Peeter")
    perenimi: str = Field(example="Meeter")
    kood: str = Field(example="12345678")
    makseviis: str = Field(example="SULARAHA")
    lisainfo: str = Field(example="Info")