from datetime import datetime
from typing import Union

from sqlalchemy import Column, String, Text, ForeignKey
from sqlalchemy.orm import relationship

from app.domain.model import külaline, ettevõte, eraisik
from app.infrastructure.sqlite.database import base
from app.query import külaline_readmodel
from app.domain.value_object import ee_isikukood, ee_registrikood

class külaline_dto(base):

    __tablename__ = "kylalised"
    yritus_uuid:   Union[str, Column] = Column(String(32),  ForeignKey('yritus.uuid', ondelete='CASCADE'), primary_key=True, autoincrement=False)
    kood:          Union[int, Column] = Column(String(11),  primary_key=True, autoincrement=False)
    nimi:          Union[str, Column] = Column(String(255), nullable=False, unique=False)
    perenimi:      Union[str, Column] = Column(String(255), nullable=False, unique=False)
    makseviis:     Union[str, Column] = Column(String(8),   nullable=False)
    lisainfo:      Union[str, Column] = Column(Text,        nullable=True)
    yritus = relationship("üritus_dto", back_populates="kylalised")

    def to_entity(self) -> külaline:
        if len(self.kood) == 8:
            isik = ettevõte(nimi=self.nimi, registrikood=ee_registrikood(self.kood))
        else:
            isik = eraisik(eesnimi=self.nimi, perekonnanimi=self.perenimi, isikukood=ee_isikukood(self.kood))
        return külaline(isik=isik, makseviis=self.makseviis, lisainfo=self.lisainfo)

    def to_read_model(self) -> külaline_readmodel:
       return külaline_readmodel(nimi=self.nimi, perenimi=self.perenimi, kood=self.kood, makseviis=self.makseviis, lisainfo=self.lisainfo)

    @staticmethod
    def from_entity(k: külaline) -> "külaline_dto":
        return külaline_dto(
            yritus_uuid=k.yritus_uuid,
            kood=k.isik.get_kood(),
            nimi=k.isik.get_nimi(),
            perenimi=k.isik.get_perekonnanimi(),
            makseviis=k.makseviis,
            lisainfo=k.lisainfo,
        )
