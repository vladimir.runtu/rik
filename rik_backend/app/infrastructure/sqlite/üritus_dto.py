from datetime import datetime
from typing import Union

from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.orm import relationship

from app.domain.model import üritus, külaline
from app.infrastructure.sqlite.database import base
from app.query import üritus_readmodel, külaline_readmodel
from .külaline_dto import külaline_dto


class üritus_dto(base):

    __tablename__ = "yritus"
    uuid: Union[str, Column]       = Column(String(32),  primary_key=True, autoincrement=False)
    nimi: Union[str, Column]       = Column(String(255), nullable=False, unique=False)
    aeg:  Union[int, Column]       = Column(Integer,     nullable=False)
    koht: Union[int, Column]       = Column(Integer,     nullable=False)
    lisainfo: Union[str, Column]   = Column(Text,        nullable=False)
    created_at: Union[int, Column] = Column(Integer,     nullable=False, index=True)
    updated_at: Union[int, Column] = Column(Integer,     nullable=False, index=True)
    kylalised = relationship(külaline_dto, back_populates="yritus", lazy="noload", cascade="all, delete, delete-orphan", passive_deletes=True)

    def to_entity(self) -> üritus:
        m = üritus(uid=self.uuid, nimi=self.nimi, aeg=datetime.fromtimestamp(self.aeg), lisainfo=self.lisainfo, koht=self.koht, created_at=datetime.fromtimestamp(self.created_at), updated_at=datetime.fromtimestamp(self.updated_at))
        for i, o in enumerate(self.kylalised):
            m.lisa_külaline(külaline_dto.to_entity(o))
        return m

    def to_read_model(self) -> üritus_readmodel:
        m = üritus_readmodel(uid=self.uuid, nimi=self.nimi, koht=self.koht, aeg=datetime.fromtimestamp(self.aeg), lisainfo=self.lisainfo, created_at=datetime.fromtimestamp(self.created_at), updated_at=datetime.fromtimestamp(self.updated_at))
        for i, o in enumerate(self.kylalised):
            m.kylalised.append(külaline_readmodel(nimi=o.nimi, perenimi=o.perenimi, kood=o.kood, makseviis=o.makseviis, lisainfo=o.lisainfo))

        return m

    @staticmethod
    def from_entity(ürr: üritus) -> "üritus_dto":
        return üritus_dto(uuid=str(ürr.uid).replace('-',''), nimi=ürr.nimi, aeg=datetime.timestamp(ürr.aeg), koht=ürr.koht, lisainfo=ürr.lisainfo, created_at=datetime.timestamp(ürr.created_at), updated_at=datetime.timestamp(ürr.updated_at))
