from typing import Optional, List
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.session import Session
from sqlalchemy.orm import joinedload
from app.domain.model import üritus, üritus_repository_interface
from .üritus_dto import üritus_dto
from .külaline_dto import külaline_dto


class üritus_repository(üritus_repository_interface):

    def __init__(self, session: Session):
        self.session: Session = session
        self.session.begin()

    def create(self, ürr: üritus):
        ürr_dto = üritus_dto.from_entity(ürr)
        try:
            self.session.add(ürr_dto)
            self.session.commit()
        except:
            raise

    def delete_by_id(self, id: str) -> None:
        try:
            self.session.query(üritus_dto).filter_by(uuid=id).delete()
            self.session.commit()
        except:
            raise


    def get_entity_by_id(self, id: str):
        try:
            ürr = self.session.query(üritus_dto).options(joinedload(üritus_dto.kylalised)).filter_by(uuid=id).one()
        except NoResultFound:
            return None
        except:
            raise

        return ürr

    def find_by_id(self, id: str):
        try:
            ürr_dto = self.session.query(üritus_dto).options(joinedload(üritus_dto.kylalised)).filter_by(uuid=id).one()
        except NoResultFound:
            return None
        except:
            raise

        return ürr_dto.to_read_model()

    def find_all(self) -> List[üritus]:
        try:
            rows = (
                self.session.query(üritus_dto)
                .order_by(üritus_dto.updated_at)
                .limit(100)
                .all()
            )
        except NoResultFound:
            return None
        except:
            raise
        if len(rows) == 0:
            return []
        return list(map(lambda üritus_dto: üritus_dto.to_read_model(), rows))
