from pydantic import BaseModel, Field
from app.domain.model import üritus
from datetime import datetime
from typing import List
from .külaline_readmodel import külaline_readmodel

class üritus_readmodel(BaseModel):

    uid: str = Field(example="5f202bf198e24242b6a11a569fd7f028")
    nimi: str = Field(example="Kohtumine")
    koht: str = Field(example="Tallinn")
    lisainfo: str = Field(example="Info")
    aeg: datetime = Field(example=1136214245000)
    created_at: datetime = Field(example=1136214245000)
    kylalised: List[külaline_readmodel] = []

    class Config:
        orm_mode = True

    @staticmethod
    def from_entity(ürr: üritus) -> "üritus_readmodel":
        return üritus_readmodel(
            uid=ürr.uid,
            nimi=ürr.nimi,
            koht=ürr.koht,
            lisainfo=ürr.lisainfo,
            aeg=ürr.aeg,
            created_at=ürr.created_at
        )
