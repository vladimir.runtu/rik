from pydantic import BaseModel, Field

class külaline_readmodel(BaseModel):

    nimi: str = Field(example="Peeter")
    perenimi: str = Field(example="Meeter")
    kood: str = Field(example="12345678")
    makseviis: str = Field(example="SULARAHA")
    lisainfo: str = Field(example="Info")

    class Config:
        orm_mode = True
