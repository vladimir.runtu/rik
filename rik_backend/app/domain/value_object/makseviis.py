from enum import Enum
from dataclasses import dataclass

@dataclass(init=False, eq=True, frozen=True)
class makseviis(Enum):
    SULARAHA = 1
    ÜLEKANNE = 2
