from dataclasses import dataclass


@dataclass(init=False, eq=True, frozen=True)
class ee_registrikood:

    value: str

    def __init__(self, value: str):

        #TODO: paranda algoritmi
        if len(value) != 8:
            raise ValueError("Viga Eesti registrikoodis")

        object.__setattr__(self, "value", value)

    def __eq__(self, o: object) -> bool:
        if isinstance(o, ee_registrikood):
            return self.value == o.value

        return False