from dataclasses import dataclass

@dataclass(init=False, eq=True, frozen=True)
class ee_isikukood:

    value: str

    def __init__(self, value: str):

        #TODO: paranda algoritmi
        if len(value) != 11:
            raise ValueError("Viga Eesti isikukoodis")

        object.__setattr__(self, "value", value)

    def __eq__(self, o: object) -> bool:
        if isinstance(o, ee_isikukood):
            return self.value == o.value

        return False