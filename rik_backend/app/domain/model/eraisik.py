from app.domain.value_object.ee_isikukood import ee_isikukood
from app.domain.model.isik_interface import isik_interface

class eraisik(isik_interface):
    def __init__(self, eesnimi: str, perekonnanimi: str, isikukood: ee_isikukood):
        self.eesnimi: str = eesnimi
        self.perekonnanimi: str = perekonnanimi
        self.isikukood: ee_isikukood = isikukood

    def get_kood(self) -> str:
        return self.isikukood;

    def get_nimi(self) -> str:
        return self.eesnimi;

    def get_perekonnanimi(self) -> str:
        return self.perekonnanimi;

    def __eq__(self, o: object) -> bool:
        if isinstance(o, eraisik):
            return self.isikukood == o.isikukood

        return False