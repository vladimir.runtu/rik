from uuid import UUID
import datetime

class entity:

    def __init__(self, uid: UUID, created_at: datetime, updated_at: datetime):
        self.uid: UUID = uid
        self.created_at: datetime = created_at
        self.updated_at: datetime = updated_at

    def get_uid(self) -> UUID:
        return self.uid

