from .eraisik import eraisik
from .ettevõte import ettevõte
from .külaline import külaline
from .üritus import üritus
from .entity import entity
from .üritus_repository_interface import üritus_repository_interface
from .isik_interface import isik_interface
