from app.domain.value_object.ee_registrikood import ee_registrikood
from app.domain.model.isik_interface import isik_interface

class ettevõte(isik_interface):
    def __init__(self, nimi: str, registrikood: ee_registrikood):
        self.nimi: str = nimi
        self.registrikood: ee_registrikood = registrikood

    def get_kood(self) -> str:
        return self.registrikood;

    def get_nimi(self) -> str:
        return self.nimi;

    def get_perekonnanimi(self) -> str:
        return "";

    def __eq__(self, o: object) -> bool:
        if isinstance(o, ettevõte):
            return self.registrikood == o.registrikood

        return False