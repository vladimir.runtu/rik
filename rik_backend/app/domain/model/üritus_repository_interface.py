from abc import ABC, abstractmethod
from typing import List, Optional

from app.domain.model import üritus
from app.query import üritus_readmodel

class üritus_repository_interface(ABC):

    @abstractmethod
    def create(self, ürr: üritus) -> Optional[üritus]:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, id: str) -> None:
        raise NotImplementedError

    @abstractmethod
    def find_by_id(self, id: str):
        raise NotImplementedError

    @abstractmethod
    def find_all(self) -> list[üritus]:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, id: str):
        raise NotImplementedError