from abc import ABC, abstractmethod

class isik_interface(ABC):

    @abstractmethod
    def get_kood(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def get_nimi(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def get_perekonnanimi(self) -> str:
        raise NotImplementedError
