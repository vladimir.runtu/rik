from abc import ABC, abstractmethod
from typing import List, Optional
import datetime

from app.domain.model import külaline, üritus

class külaline_repository_interface(ABC):

    @abstractmethod
    def add_to_event(self, event_uid: str, k: külaline) -> Optional[üritus]:
        raise NotImplementedError

    def remove_from_event(self, event_uid: str, guest_uid: str) -> Optional[üritus]:
        raise NotImplementedError
