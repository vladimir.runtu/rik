from uuid import UUID
from datetime import datetime
from app.domain.model.külaline import külaline
from app.domain.model.entity import entity

class üritus(entity):
    def __init__(self, uid: UUID, nimi: str, aeg: datetime, koht: str, lisainfo: str, created_at: datetime, updated_at: datetime):

        super().__init__(uid=uid, created_at=created_at, updated_at=updated_at)
        self.nimi: str = nimi
        self.aeg: datetime = aeg
        self.koht: str = koht
        self.lisainfo: str = lisainfo
        self.külalised: list[külaline] = []

    def __find_külaline_index(self, külaline: külaline) -> int:
        for i, o in enumerate(self.külalised):
            if o == külaline:
                return i
        return -1

    def lisa_külaline(self, külaline: külaline) -> bool:
        idx = self.__find_külaline_index(külaline=külaline)
        if (idx < 0):
            self.külalised.append(külaline)
            return True
        return False

    def kustuta_külaline(self, külaline: külaline) -> None:
        idx = self.__find_külaline_index(külaline=külaline)
        if (idx >= 0):
            del self.külalised[idx]

    def külaliste_nimekiri(self) -> list[külaline]:
        return list(self.külalised)

    def külaliste_arv(self) -> int:
        return len(self.külalised)