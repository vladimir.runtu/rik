from app.domain.model import ettevõte, eraisik

class külaline:
    def __init__(self, isik: eraisik | ettevõte, makseviis: str, lisainfo: str):
        self.isik: eraisik | ettevõte = isik
        self.makseviis: str = makseviis
        self.lisainfo: str = lisainfo

    def __eq__(self, o: object) -> bool:
        if isinstance(o, külaline):
            return self.isik == o.isik