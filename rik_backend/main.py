import logging
from logging import config
from typing import Iterator, List
import uuid
from app.query import üritus_readmodel
from typing import Optional, cast
from datetime import datetime
from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware

from app.domain.model import üritus_repository_interface, üritus, külaline, eraisik, ettevõte
from app.domain.value_object import ee_isikukood, ee_registrikood
from app.command import üritus_createmodel, külaline_createmodel

from sqlalchemy.orm.session import Session
from app.infrastructure.sqlite.database import SessionLocal, create_tables
from app.infrastructure.sqlite import üritus_repository, külaline_dto
def get_session() -> Iterator[Session]:
    session: Session = SessionLocal()
    try:
        yield session
    finally:
        session.close()

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

create_tables()

@app.get("/yritus", response_model=List[üritus_readmodel], status_code=status.HTTP_200_OK)
async def üritused(sess: Session = Depends(get_session)):
    try:
        repo: üritus_repository_interface = üritus_repository(sess)
        rows = repo.find_all()
    except:
        raise
    if len(rows) == 0:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Üritusi ei leidunud",
        )
    return rows

@app.post("/yritus", response_model=üritus_readmodel, status_code=status.HTTP_201_CREATED)
async def lisa_üritus(data: üritus_createmodel, sess: Session = Depends(get_session)) -> Optional[üritus_readmodel]:
    try:
        uid = uuid.uuid4()
#        if (datetime.today() > data.aeg):
#            raise ValueError("Ürituse toimumise aeg peab olema tulevikus")

        ürr = üritus(uid=uid, nimi=data.nimi, aeg=data.aeg, koht=data.koht, lisainfo=data.lisainfo, created_at=datetime.now(), updated_at=datetime.now())
        repo: üritus_repository_interface = üritus_repository(sess)
        repo.create(ürr)

        created_ürr = repo.find_by_id(str(uid).replace('-',''))
    except:
        raise

    return üritus_readmodel.from_entity(cast(üritus, created_ürr))

@app.post("/yritus/{yritus_id}/kylaline", response_model=üritus_readmodel, status_code=status.HTTP_201_CREATED)
async def lisa_külaline(yritus_id: str, data: külaline_createmodel, sess: Session = Depends(get_session)) -> Optional[üritus_readmodel]:
    try:
        if len(data.kood) == 8:
            isik = ettevõte(nimi=data.eesnimi, registrikood=ee_registrikood(data.kood))
        elif len(data.kood) == 11:
            isik = eraisik(eesnimi=data.eesnimi, perekonnanimi=data.perenimi, isikukood=ee_isikukood(data.kood))
        else:
            raise ValueError("Viga isikukoodis")

        repo: üritus_repository_interface = üritus_repository(sess)

        k = külaline(isik=isik, makseviis=data.makseviis, lisainfo=data.lisainfo)
        ürr = repo.get_entity_by_id(yritus_id)
        ret = None
        if (ürr != None):
            ent = ürr.to_entity()
            if ent.lisa_külaline(k) == True:
                ürr.kylalised.append(külaline_dto(yritus_uuid = yritus_id, kood=data.kood, perenimi=data.perenimi, nimi=data.eesnimi, makseviis=data.makseviis, lisainfo=data.lisainfo))
                sess.commit()
                ret = repo.find_by_id(yritus_id)
            else:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Antud isik on juba külalisena registreeritud")
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Sellise koodiga üritusi ei leidunud")

        return ret
    except ValueError as ve:
        raise HTTPException(status_code=status.HTTP_406_NOT_ACCEPTABLE, detail="Viga isik- või registrikoodis")
    except:
        raise

@app.get("/yritus/{yritus_id}", response_model=üritus_readmodel, status_code=status.HTTP_200_OK)
async def get_üritus(yritus_id: str, sess: Session = Depends(get_session)) -> Optional[üritus_readmodel]:
    try:
        repo: üritus_repository_interface = üritus_repository(sess)
        ürr = repo.find_by_id(yritus_id)
    except:
        raise

    if (ürr == None):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Sellise koodiga üritusi ei leidunud")

    return ürr

@app.delete("/yritus/{yritus_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_üritus(yritus_id: str, sess: Session = Depends(get_session)) -> None:
    try:
        repo: üritus_repository_interface = üritus_repository(sess)
        repo.delete_by_id(yritus_id)
    except:
        raise

    return None